Change list from the previous version:
* Update build-package.sh reuse variable in sed command
* [ci/scripts/jobs/build-package] Add release file sanity check
* [tests/run-testsuite] Minor updates
* [gitlabci_yml2bash] Add `inherit` keyword support
* [gitlabci_yml2bash] Add support of local include with wildcard
* [tests] Add yaml2bash tests
