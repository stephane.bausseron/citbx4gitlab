#!/usr/bin/env python3
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2021 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""CI pipeline processor"""

import os
import sys

import yaml

if "CI_PROJECT_DIR" not in os.environ:
    sys.exit()
CI_PROJECT_DIR = os.environ.get("CI_PROJECT_DIR")

class Error(Exception):
    """Error class"""

def read_ci_pipeline_yml(ci_pipeline_path):
    """Read a .ci-pipeline.yml file"""
    try:
        file_d = open(ci_pipeline_path)
        document = yaml.safe_load(file_d.read())
        if not isinstance(document, dict):
            raise Error("Invalid document format")
        return document

    except Exception as err:
        raise Error("Unable to load GitLab-CI YAML [" + ci_pipeline_path + "] file!" \
            + os.linesep + os.linesep + "Raison:" + os.linesep + str(err))

def shell_str(text):
    """Escape a string character to be evaluated in bash os.environment"""
    return "'" + text.replace("'", "'\\''") + "'"

def compute_arg(name, value):
    """Argument list generator"""
    if isinstance(value, list):
        result = ""
        for elt in value:
            result += compute_arg(name, elt)
        return result
    if isinstance(value, bool):
        if value:
            return " --" + name + " true"
        return " --" + name + " false"
    return " --" + name + " " + shell_str(str(value))

def process_pipeline(document, pipeline_name):
    """Process a specific pipeline"""
    if not pipeline_name in document:
        return
    if not isinstance(document[pipeline_name], list):
        raise Error("Node [" + pipeline_name + "] must be an array")
    citbx_args_list = []
    for job in document[pipeline_name]:
        if isinstance(job, str):
            job_node = {'name': job, 'arguments': {}}
        else:
            if not 'name' in job:
                raise Error("Missing [name] property in one job item")
            if not isinstance(job['name'], str):
                raise Error("[name] property must be a charracter string")
            if 'arguments' in job and not isinstance(job['arguments'], dict):
                raise Error("[arguments] property must be an object")
            job_node = job
        citbx_args = job_node['name']
        for key, value in job_node['arguments'].items():
            citbx_args += compute_arg(key, value)
        citbx_args_list.append(citbx_args)
    print("CITBX_PIPELINE_ARGS_LIST=()")
    for citbx_args in citbx_args_list:
        print("CITBX_PIPELINE_ARGS_LIST+=(" + shell_str(citbx_args) + ")")

def list_pipeline(document):
    """Generate the CITBX_PIPELINE_LIST bash list"""
    print("CITBX_PIPELINE_LIST=()")
    for key in document:
        print("CITBX_PIPELINE_LIST=(" + shell_str(key) + ")")

def main():
    """Main function"""
    if "CITBX_PIPELINES_CONFIG_PATH" in os.environ:
        ci_pipeline_path = CI_PROJECT_DIR + '/' + os.environ.get("CITBX_PIPELINES_CONFIG_PATH")
    else:
        ci_pipeline_path = CI_PROJECT_DIR + '/' + ".ci-pipeline.yml"
    try:
        if not os.path.isfile(ci_pipeline_path):
            sys.exit(0)
        document = read_ci_pipeline_yml(ci_pipeline_path)
        if "CITBX_PIPELINE_NAME" in os.environ:
            pipeline_name = os.environ.get("CITBX_PIPELINE_NAME")
            process_pipeline(document, pipeline_name)
        else:
            list_pipeline(document)
    except Error as err:
        print("print_critical", end=' ')
        for line in str(err).splitlines():
            print(shell_str(line), end=' ')
        print("")


if __name__ == "__main__":
    main()
